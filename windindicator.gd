extends Node3D


@onready var SHIP = $"../ship"
func _process(delta):
	transform.origin = SHIP.transform.origin
	rotation.y = SHIP.WIND.angle()
