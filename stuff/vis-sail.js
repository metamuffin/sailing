// vis-sail.ts
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
var SCALE = 100;
canvas.style.position = "absolute";
canvas.style.top = "0px";
canvas.style.left = "0px";
canvas.style.width = "100vw";
canvas.style.height = "100vh";
document.body.append(canvas);
function resize() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}
globalThis.addEventListener("resize", resize);
resize();
var mouse = { x: 0, y: 0 };
document.addEventListener("mousemove", (ev) => {
  mouse.x = (ev.clientX - canvas.width / 2) / SCALE;
  mouse.y = (ev.clientY - canvas.height / 2) / SCALE;
});
var sail_angle = 1;
var wind = { x: 0, y: -1.2 };
var ZERO = { x: 0, y: 0 };
function line(a, b, color = "white", vector = true) {
  ctx.strokeStyle = color;
  ctx.beginPath();
  ctx.moveTo(a.x, a.y);
  ctx.lineTo(b.x, b.y);
  if (vector)
    ctx.arc(b.x, b.y, 0.1, 0, Math.PI * 2);
  ctx.stroke();
}
function dot(a, b) {
  return a.x * b.x + a.y * b.y;
}
function scale(a, f) {
  return { x: a.x * f, y: a.y * f };
}
function sub(a, b) {
  return { x: a.x - b.x, y: a.y - b.y };
}
function add(a, b) {
  return { x: a.x + b.x, y: a.y + b.y };
}
function perp(a) {
  return { x: a.y, y: -a.x };
}
function project(v, n) {
  return scale(v, dot(n, v));
}
function reflect(v, n) {
  return sub(scale(project(perp(n), v), 2), v);
}
function redraw() {
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.save();
  ctx.translate(canvas.width / 2, canvas.height / 2);
  ctx.scale(SCALE, SCALE);
  ctx.lineWidth = 0.05;
  const sail = { x: Math.cos(sail_angle), y: Math.sin(sail_angle) };
  wind = scale(mouse, -1);
  line(scale(wind, -1), ZERO, "blue");
  line(ZERO, sail, "magenta");
  line(scale(perp(sail), 2), scale(perp(sail), -2), "#880088", false);
  const ref = reflect(wind, sail);
  line(ZERO, wind, "#004400");
  line(ZERO, ref, "#004400");
  const k = 0.6;
  const exit = add(scale(wind, 1 - k), scale(ref, k));
  line(ZERO, exit, "#00ff00");
  const force = sub(wind, exit);
  line(ZERO, force, "#ff0000");
  const eforce = { x: force.x, y: force.y * 0.2 };
  line(ZERO, eforce, "#880000");
  const force2 = scale(sail, dot(wind, sail));
  line(ZERO, force2, "#ffff00");
  ctx.restore();
  requestAnimationFrame(redraw);
}
redraw();
