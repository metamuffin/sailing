extends RigidBody3D


const WIND = Vector2(20, 0)
const MOTOR_SPEED = 4

@onready var SAIL = $sail
@onready var BAUM = $baum
@onready var DEBUG = $"../hud/debug"

@export var curve_fw: Curve
@export var curve_side: Curve

@export var sail_size = 200.
@export var weight = 30.

var sail_angle = PI / 2.
var mainsheet = PI / 2.
var rudder = 0;

func _ready():
	var collider = $hull/StaticBody3D/CollisionShape3D
	var t = collider.global_transform
	$hull/StaticBody3D.remove_child(collider)
	self.add_child(collider)
	collider.set_owner(self)
	collider.global_transform = t

func _physics_process(delta):
	var mainsheet_control = Input.get_axis("sail_close", "sail_far")
	var rudder_control = Input.get_axis("steer_left", "steer_right")
	
	var vel = Vector2(linear_velocity.x, linear_velocity.z).rotated(rotation.y)
	var wind = WIND.rotated(rotation.y) - vel
	
	mainsheet += mainsheet_control * 0.5 * delta
	mainsheet = clamp(mainsheet, 0, PI / 2)
	sail_angle += wind.rotated(sail_angle).x * delta * 0.2
	sail_angle = clamp(sail_angle, -mainsheet, mainsheet)

	var sail = Vector2.from_angle(-sail_angle)
	var sail_force = sail * sail.dot(wind) * cos(rotation.z) * sail_size
	
	apply_force(
		Vector3(sail_force.x, 0, sail_force.y).rotated(Vector3.UP, rotation.y),
		transform.origin + Vector3(0, 5, 0)
	)
	apply_force(Vector3(WIND.x * 50, 0, WIND.y * 50)) # hull drag

#	apply_impulse(
#		Vector3(rotation.z * 10000, 0, 0).rotated(Vector3.UP, -rotation.y),
#		transform.origin + Vector3(0, 10, 0)
#	)
#	apply_impulse(
#		Vector3(rotation.z * -10000, 0, 0).rotated(Vector3.UP, -rotation.y),
#		transform.origin + Vector3(0, -10, 0)
#	)
#	apply_torque(Vector3(0,0,rotation.z * -100))

	rudder += (rudder_control - rudder) * 0.1 * delta
	angular_velocity.y += rudder * vel.y * 5 * delta

	var sail_bend = sail_force.x * -0.0002;
	var sail_wobble = max(WIND.rotated(sail_angle).x, 0)

	DEBUG.text = "
	mainsheet=%f
	sail_angle=%f
	rotation=(%f, %f, %f)
	vel = (%f, %f)
	wind = (%f, %f)
	sail_force = (%f, %f)
	rudder=%f
	sail_bend = %f
	sail_wobble = %f
	" % [mainsheet, sail_angle, rotation.x, rotation.y,rotation.z,  vel.x, vel.y, wind.x, wind.y, sail_force.x, sail_force.y, rudder, sail_bend, sail_wobble]

	SAIL.rotation.y = sail_angle
	BAUM.rotation.y = sail_angle
	SAIL.set_instance_shader_parameter("wobble", sail_wobble);
	SAIL.set_instance_shader_parameter("side", sail_bend);
	
	vel = Vector2(linear_velocity.x, linear_velocity.z).rotated(rotation.y)
	vel.x -= delta * sign(vel.x) * vel.x**2 * 90 / weight # sidewards friction
	vel.y -= delta * sign(vel.y) * vel.x**2 * 5 / weight # forward friction
	vel = vel.rotated(-rotation.y)
	
	linear_velocity.x = vel.x
	linear_velocity.z = vel.y
	linear_velocity.y = -min(transform.origin.y,0) * delta * 100.
