extends RigidBody3D


func _ready():
	pass

func _physics_process(delta):
	var walk_control = Input.get_vector("walk_left","walk_right","walk_forward","walk_backward")
	
	angular_velocity.x += rotation.x * delta * -100.
	angular_velocity.z += rotation.z * delta * -100.
	angular_velocity.y = 0
