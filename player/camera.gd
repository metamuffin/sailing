extends Camera3D

var offset
@onready var BODY = $"../body"

func _ready():
	offset = transform.origin - BODY.transform.origin

func _physics_process(delta):
	var target = BODY.transform.origin + (BODY.linear_velocity * 4.) + BODY.transform.basis * offset;	
	transform.origin += (target - transform.origin) * (1 - pow(0.8, delta))
	look_at(BODY.transform.origin)
